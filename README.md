# OpenML dataset: strikes

https://www.openml.org/d/549

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Bruce Western (western@datacomm.iue.it)   
**Source**: [StatLib](http://lib.stat.cmu.edu/datasets/) - 1999  
**Please cite**:   

The data consist of annual observations on the level of strike volume (days lost due to industrial disputes per 1000 wage salary earners), and their covariates in 18 OECD countries from 1951-1985. The average level and variance of strike volume varies across countries. The data distribution also features a long right tail and several large outliers. 

The 7 data fields include the following variables:  
>
(1) country code;  
(2) year;  
(3) strike volume;  
(4) unemployment;  
(5) inflation;  
(6) parliamentary representation of social democratic and labor parties  
(7) a time-invariant measure of union centralization.

These data were analyzed in the forthcoming paper by Bruce Western, "Vague Theory and Model Uncertainty in Macrosociology," which is to appear in Sociological Methodology. Permission is given by the author to freely use and redistribute these data.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/549) of an [OpenML dataset](https://www.openml.org/d/549). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/549/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/549/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/549/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

